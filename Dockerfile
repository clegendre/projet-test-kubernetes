FROM nginx:latest

# Copy the nginx configuration file
COPY test.html /usr/share/nginx/html

# Expose the port 80
EXPOSE 80

# Run the nginx server
CMD ["nginx", "-g", "daemon off;"]
